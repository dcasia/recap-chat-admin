const path = require('path')
require('dotenv').config()
const PrerenderSPAPlugin = require('prerender-spa-plugin')

module.exports = {
    css: {
        loaderOptions: {
            sass: {
                implementation: require('sass'),
                prependData: `@import "./scss/global.scss";`,
                sassOptions: {
                    includePaths: [
                        path.resolve(__dirname, 'source')
                    ]
                }
            }
        }
    },
    chainWebpack: config => {


        config.module.rule('eslint').use('eslint-loader').options({ fix: true })

    },
    configureWebpack: config => {

        if (process.env.NODE_ENV === 'production') {

            return {
                plugins: [
                    new PrerenderSPAPlugin({
                        staticDir: path.join(__dirname, 'distribution'),
                        routes: [ '/about', '/get-started' ]
                    })
                ]
            }

        }

        return {}

    },
    pages: {
        index: {
            entry: 'source/Main.ts'
        }
    },
    outputDir: 'distribution',
    devServer: {
        proxy: {
            '/rest': {
                target: `https://${ process.env.API_DOMAIN }`,
                changeOrigin: true,
                secure: false,
                pathRewrite: { '^/rest': '' },
                logLevel: 'debug'
            }
        }
    }
}

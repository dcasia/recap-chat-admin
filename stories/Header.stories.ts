import { text } from '@storybook/addon-knobs'
import Header from '../source/components/Header.vue'
import ArrowIcon from '../source/components/icons/ArrowIcon.vue'

export default {
    title: 'Components',
    component: Header
}

const options = (options = {}) => ({
    title: {
        default: text('Title', 'IKEA Prototype Testing')
    },
    subtitle: {
        default: text('Subtitle', '4 of 10 entries received')
    },
    navigation: {
        default: {
            to: '#',
            title: text('Navigation Text', 'Dashboard')
        }
    },
    ...options
})

export const header = () => ({
    components: { Header, ArrowIcon },
    props: options(),
    template: `
        <Header v-bind="$props"/>
    `
})
import { boolean, number, select, text } from '@storybook/addon-knobs'
import ArrowIcon from '../source/components/icons/ArrowIcon.vue'
import Input from '../source/components/Input.vue'
import { Color } from '../source/enums/Color'
import { InputType } from '../source/enums/InputType'



export default {
    title: 'Components',
    component: Input,
    decorators: [
    ]
}

const options = (options = {}) => ({
    placeholder: {
        default: text('Placeholder', 'Name')
    },
    hollow: {
        default: boolean('Hollow', false)
    },
    disabled: {
        default: boolean('Disabled', false)
    },
    color: {
        default: select('Color', Color, Color.Primary)
    },
    type: {
        default: select('Type', InputType, InputType.Email)
    },
    fontSize: {
        default: number('Font Size', 18, {
            range: true,
            min: 10,
            max: 90,
            step: 1
        })
    },
    ...options
})

export const input = () => ({
    components: { Input, ArrowIcon },
    props: options(),
    setup() {
        return {}
    },
    template: `
        <Input :style="{fontSize: fontSize + 'px'}" :color="color" name="demo" :type="type" :placeholder="placeholder" :style="{ fontSize: fontSize + 'px' }"/>
    `
})

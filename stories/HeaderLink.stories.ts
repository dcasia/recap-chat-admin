import { text } from '@storybook/addon-knobs'
import HeaderLink from '../source/components/HeaderLink.vue'

export default {
    title: 'Components',
    component: HeaderLink
}

const options = (options = {}) => ({
    title: {
        default: text('Title', 'IKEA Prototype Test 01 ')
    },
    subtitle: {
        default: text('Subtitle', '10 Participants')
    },
    ...options
})

export const headerLink = () => ({
    components: { HeaderLink },
    props: options(),
    template: `
        <HeaderLink v-bind="$props"/>
    `
})
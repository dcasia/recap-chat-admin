import { boolean, number, radios, select, text, withKnobs } from '@storybook/addon-knobs'
import { withDesign } from 'storybook-addon-designs'
import Card from '../source/components/Card.vue'
import Container from '../source/components/Container.vue'
import ArrowIcon from '../source/components/icons/ArrowIcon.vue'
import { Color } from '../source/enums/Color'
import { Direction } from '../source/enums/Direction'
import { Size } from '../source/enums/Size'

export default {
    title: 'Components',
    component: Card,
}

enum Placement {
    None = 'none',
    Left = 'left',
    Right = 'right',
    Both = 'both',
}

const options = (options = {}) => ({
    fluid: { default: boolean('Fluid', false) },
    center: { default: boolean('Center', false) },
    ...options
})

export const card = () => ({
    components: { Card },
    props: options(),
    setup() {
        return {
            Direction,
            Placement
        }
    },
    template: `
        <Card v-bind="$props">
            This is a card
        </Card>
    `
})

import { select } from '@storybook/addon-knobs'
import Tag from '../source/components/Tag.vue'
import { ProjectTaskStatus } from '../source/enums/ProjectTaskStatus'

export default {
    title: 'Components',
    component: Tag
}

export const tag = () => ({
    components: { Tag },
    props: {
        type: {
            default: select('Type', ProjectTaskStatus, ProjectTaskStatus.Completed)
        }
    },
    template: `
        <Tag v-bind="$props"/>
    `
})
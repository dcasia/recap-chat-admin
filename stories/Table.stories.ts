import Table from '../source/components/Table.vue'

export default {
    title: 'Components',
    component: Table
}

export const table = () => ({
    components: { Table },
    setup() {
        return {
            tableData: {
                withCheckboxes: true,
                headers: [ null, 'Name', 'Status', 'Location', 'Gender', 'Age', null ],
                body: [
                    {
                        name: 'Test',
                        status: 'Recruited',
                        location: 'Shanghai',
                        gender: 'Male',
                        age: 32
                    },
                    {
                        name: 'Test',
                        status: 'Recruited',
                        location: 'Shanghai',
                        gender: 'Male',
                        age: 32
                    }
                ]
            }
        }
    },
    template: `
        <Table :data="tableData"/>
    `
})

import { boolean, number, radios, select, text } from '@storybook/addon-knobs'
import Button from '../source/components/Button.vue'
import Container from '../source/components/Container.vue'
import ArrowIcon from '../source/components/icons/ArrowIcon.vue'
import { Color } from '../source/enums/Color'
import { Direction } from '../source/enums/Direction'
import { Size } from '../source/enums/Size'

export default {
    title: 'Components',
    component: Button
}

enum Placement {
    None = 'none',
    Left = 'left',
    Right = 'right',
    Both = 'both',
}

const options = (options = {}) => ({
    content: {
        default: text('Content', 'Button')
    },
    hollow: {
        default: boolean('Hollow', false)
    },
    disabled: {
        default: boolean('Disabled', false)
    },
    color: {
        default: select('Color', Color, Color.Primary)
    },
    size: {
        default: select('Size', Size, Size.Normal)
    },
    iconPlacement: {
        default: radios('Icon Placement', Placement, Placement.None)
    },
    fontSize: {
        default: number('Font Size', 18, {
            range: true,
            min: 10,
            max: 90,
            step: 1
        })
    },
    ...options
})

export const button = () => ({
    components: { Button, ArrowIcon },
    props: options(),
    setup() {
        return {
            Direction,
            Placement
        }
    },
    template: `
        <Button v-bind="$props">

            <template #left-icon v-if="[ Placement.Left, Placement.Both ].includes(iconPlacement)">
                <ArrowIcon/>
            </template>

            {{ content }}

            <template #right-icon v-if="[ Placement.Right, Placement.Both ].includes(iconPlacement)">
                <ArrowIcon :direction="Direction.Right"/>
            </template>

        </Button>
    `
})

export const buttonPlusButton = () => ({
    components: { Button, Container, ArrowIcon },
    props: options(),
    setup() {
        return {
            Direction,
            Placement
        }
    },
    template: `
        <Container :vertical="false">

            ${ button().template }
            ${ button().template }
            ${ button().template }

        </Container>
    `
})

buttonPlusButton.story = {
    name: 'Button + Button'
}

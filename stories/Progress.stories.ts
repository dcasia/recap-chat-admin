import Progress from '../source/components/Progress.vue'
import { ProjectTaskStatus } from '../source/enums/ProjectTaskStatus'

export default {
    title: 'Components',
    component: Progress
}

export const progress = () => ({
    components: { Progress },
    setup() {
        return {
            data: [
                {
                    type: ProjectTaskStatus.Completed,
                    progress: 10
                },
                {
                    type: ProjectTaskStatus.StartedTest,
                    progress: 10
                },
                {
                    type: ProjectTaskStatus.Recruited,
                    progress: 10
                },
                {
                    type: ProjectTaskStatus.Recruiting,
                    progress: 70
                }
            ]
        }
    },
    template: `
        <Progress :data="data" v-bind="$props"/>
    `
})

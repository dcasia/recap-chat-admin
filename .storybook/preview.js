import { addDecorator, addParameters } from '@storybook/vue' // <- or your storybook framework
import { withKnobs } from '@storybook/addon-knobs'
import '../source/scss/main.scss'

addParameters({
    backgrounds: [
        { name: 'Black', value: '#000' },
        { name: 'White', value: '#fff', default: true },
        { name: 'RecapChat Body', value: '#F7F8FB' },
        { name: 'Primary', value: '#4378FF' },
    ]
})

// addDecorator(centered)
addDecorator(withKnobs)

function withCssVariables() {
    return {
        template: `

            <style>
                :root {
                    --small-spacing: 10px;
                    --medium-spacing: 48px;
                    --spacing-35: 35px;
                    --color-white: #ffffff;
                    --color-primary: #4378FF;
                    --color-primary-dark: #2E5DD3;
                    --color-black: #000000;
                }

                .center-helper {
                    position: fixed;
                    top: 0px;
                    left: 0px;
                    bottom: 0px;
                    right: 0px;
                    display: flex;
                    align-items: center;
                    overflow: auto;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    padding: 18px;
                }
                
            </style>

            <div class="center-helper">
                <story/>
            </div>

        `
    }
}

addDecorator(withCssVariables)

const path = require('path')

module.exports = {
    stories: [ '../stories/**/*.stories.ts' ],
    addons: [
        '@storybook/addon-actions',
        '@storybook/addon-links',
        '@storybook/addon-knobs/register',
        '@storybook/addon-viewport/register',
        '@storybook/addon-design-assets',
        '@storybook/addon-backgrounds/register',
        'storybook-addon-designs/register',
    ],
    webpackFinal: (config) => {

        config.module.rules.push({
            test: /\.scss$/,
            use: [
                'style-loader',
                'css-loader',
                {
                    loader: 'postcss-loader',
                    options: {
                        ident: 'postcss',
                    },
                },
                {
                    loader: 'sass-loader',
                    options: {
                        implementation: require('sass'),
                        prependData: `@import "./scss/global.scss";`,
                        sassOptions: {
                            includePaths: [
                                path.resolve(__dirname, '../source')
                            ]
                        }
                    }
                },
            ]

        })

        return config

    }

}

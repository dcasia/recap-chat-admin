declare module '*.vue' {
    import { ComponentOptions } from 'vue'
    const component: ComponentOptions
    export default component
}

type Writeable<T> = { -readonly [P in keyof T]: T[P] }

import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import { Route } from './enums/Route'

const routes: RouteRecordRaw[] = [

    /**
     * Site routes
     */
    {
        path: '/',
        name: Route.Home,
        component: () => import('./views/site/Home.vue')
    },
    {
        path: '/get-started',
        name: Route.GetStarted,
        component: () => import('./views/site/GetStarted.vue')
    },

    /**
     * App Routes
     */
    {
        path: '/login',
        name: Route.Login,
        component: () => import('./views/Login.vue')
    },
    {
        path: '/projects',
        name: Route.Projects,
        component: () => import('./views/Projects.vue')
    },
    {
        path: '/project/:project',
        name: Route.Project,
        component: () => import('./views/Project.vue')
    },
    {
        path: '/project/:project/:tester',
        name: Route.Submission,
        component: () => import('./views/Submission.vue')
    }

]

export default createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

import { onMounted, onUnmounted, ref, watchEffect } from 'vue'
import { useTimeline } from './UseTimeline'
// import video from '../assets/video.mp4'

/**
 * Detect if video is currently playing
 *
 * @param video
 */
const isVideoPlaying = (video: HTMLVideoElement) => !!(video.currentTime > 0 && !video.paused && !video.ended && video.readyState > 2)

function preloadVideo(source: string): Promise<string> {

    return new Promise((resolve, reject) => {

        const request = new XMLHttpRequest()
        request.open('GET', source, true)
        request.responseType = 'blob'

        request.onload = () => {
            if (request.status === 200) {
                resolve(URL.createObjectURL(request.response))
            }
        }
        request.onerror = function () {
            // Error
            reject()
        }

        request.send()

    })

}

export function useVideoPlayback() {

    const videoPlayerRef = ref<HTMLVideoElement>(null!)
    const loading = ref(true)
    const playStatus = ref(false)
    const muteStatus = ref(false)

    const { onSeek, updateProgress: updateProgressTimeline, progress, markerClick } = useTimeline()

    onMounted(() => {

        loading.value = false

        const videoElement = videoPlayerRef.value

        const onPlay = async () => (onTimeUpdate())
        const onPause = () => (onTimeUpdate())
        const onMetaData = () => (onTimeUpdate())
        const updateProgress = () => updateProgressTimeline((videoElement.currentTime / videoElement.duration || 0) * 100)
        const onTimeUpdate = () => {
            requestAnimationFrame(updateProgress)
        }

        videoElement.addEventListener('timeupdate', updateProgress, false)
        videoElement.addEventListener('loadeddata', onMetaData, false)
        videoElement.addEventListener('play', onPlay, false)
        videoElement.addEventListener('pause', onPause, false)
        videoElement.addEventListener('seeking', onTimeUpdate, false)
        videoElement.addEventListener('seeked', onTimeUpdate, false)

        function updateVideoPlayback(value: number) {
            requestAnimationFrame(() => {
                videoPlayerRef.value.currentTime = (videoPlayerRef.value.duration / 100) * value
            })
        }

        watchEffect(() => {

            if (markerClick.value) {

                updateVideoPlayback(progress.value)

            }

        })

        onSeek(value => updateVideoPlayback(value))

        /**
         * Clean up
         */
        onUnmounted(() => {

            videoElement.removeEventListener('timeupdate', updateProgress, false)
            videoElement.removeEventListener('loadeddata', onMetaData, false)
            videoElement.removeEventListener('play', onPlay, false)
            videoElement.removeEventListener('pause', onPause, false)
            videoElement.removeEventListener('seeking', onTimeUpdate, false)
            videoElement.removeEventListener('seeked', onTimeUpdate, false)

        })

    })

    return {
        loading,
        videoPlayerRef,
        playerBackStatus: playStatus,
        muteStatus,
        seek(completion: number) {

            requestAnimationFrame(() => {
                videoPlayerRef.value.currentTime = (videoPlayerRef.value.duration / 100) * completion
            })

        },
        mute() {
            muteStatus.value = videoPlayerRef.value.muted = !videoPlayerRef.value.muted
        },
        async play() {

            if (isVideoPlaying(videoPlayerRef.value)) {

                videoPlayerRef.value.pause()
                playStatus.value = false

            } else {

                await videoPlayerRef.value.play()
                playStatus.value = true

            }

        }
    }

}

export function formatTime(seconds: number): string {

    const hour = Math.floor(seconds / 3600)
    const minute = Math.floor((seconds % 3600) / 60)
    const second = Math.round(seconds % 60)

    return [
        hour,
        minute > 9 ? minute : (hour ? '0' + minute : minute || '0'),
        second > 9 ? second : '0' + second
    ].filter(Boolean).map(item => item.toString().padStart(2, '0')).join(':')

}
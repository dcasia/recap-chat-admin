import { req } from '../UseValidators'
import { Rule } from './Rule'

export class Required extends Rule {

    public validate(value: string | number | null): boolean {

        if (typeof value === 'string') {

            return req(value.trim())

        }

        return req(value)

    }

}

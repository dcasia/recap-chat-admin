import { computed, reactive } from 'vue'
import { formatTime } from './formatTime'

interface TimelineInterface {
    progress: number,
    markerClick: boolean,
    listeners: ((progress: number) => void)[]
}

const timeline = reactive<TimelineInterface>({
    progress: 0,
    markerClick: false,
    listeners: []
})

export function useTimeline(duration = 0) {
    return {
        currentTime: computed(() => formatTime((duration * timeline.progress) / 100)),
        markerClick: computed(() => timeline.markerClick),
        progress: computed(() => timeline.progress),
        updateProgress: (value: number) => timeline.progress = Math.max(value, 0),
        onTimeMarkerClick(value: number) {

            timeline.markerClick = true
            timeline.progress = value

            requestAnimationFrame(() => {
                timeline.markerClick = false
            })

        },
        onSeek: (callback: TimelineInterface['listeners'][0]) => {

            timeline.listeners.push(callback)

        },
        seek() {

            for (const listener of timeline.listeners) {

                listener(timeline.progress)

            }

        }
    }
}

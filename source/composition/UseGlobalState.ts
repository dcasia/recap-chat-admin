import { reactive } from 'vue'

export const appState = reactive({
    isAuthenticated: false
})

export function useGlobalState() {

    return {
        appState,
        updateAuthenticationStatus(status: boolean): boolean {
            return appState.isAuthenticated = status
        }
    }

}

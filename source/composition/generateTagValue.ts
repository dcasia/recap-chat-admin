import { ProjectTaskStatus } from '../enums/ProjectTaskStatus'

export function generateTagValue(type: ProjectTaskStatus): { text: string, classes: string[] } {

    if (type === ProjectTaskStatus.Completed) {
        return {
            text: 'Completed',
            classes: [ 'bg-success-light', 'border-success', 'text-success' ]
        }
    }

    if (type === ProjectTaskStatus.Processing) {
        return {
            text: 'Processing',
            classes: [ 'bg-success-light', 'border-success', 'text-success' ]
        }
    }

    if (type === ProjectTaskStatus.StartedTest) {
        return {
            text: 'Started Test',
            classes: [ 'bg-warning-light', 'border-warning', 'text-warning' ]
        }
    }

    if (type === ProjectTaskStatus.Incomplete) {
        return {
            text: 'Incomplete',
            classes: [ 'bg-danger-light', 'border-danger', 'text-danger' ]
        }
    }

    if (type === ProjectTaskStatus.Recruiting) {
        return {
            text: 'Recruiting',
            classes: [ 'bg-info-light', 'border-info', 'text-info' ]
        }
    }

    if (type === ProjectTaskStatus.Recruited) {
        return {
            text: 'Recruited',
            classes: [ 'bg-info-light', 'border-success', 'text-success' ]
        }
    }

    throw new Error('Invalid status. ' + type)

}
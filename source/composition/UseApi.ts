import { err as error, ok, Result } from 'neverthrow'
import { readonly } from 'vue'
import { ErrorCode } from '../enums/Code'
import { ServerError, SuccessInterface, SuccessLoginInterface, ValidationError } from '../interfaces/ApiInterface'
import { EnquiryInterface, ProjectListingType, ProjectType, SubmissionType } from '../interfaces/ProjectType'
import { useGlobalState } from './UseGlobalState'

const tokenCookieName = 'XSRF-TOKEN'

function getCookie(name: string): string | null {
    const match = document.cookie.match(new RegExp('(^|;\\s*)(' + name + ')=([^;]*)'))
    return match ? decodeURIComponent(match[ 3 ]) : null
}

function setCookie(name: string, value: string, days: number) {

    const date = new Date

    date.setTime(date.getTime() + 24 * 60 * 60 * 1000 * days)

    document.cookie = name + '=' + value + ';path=/;expires=' + date.toUTCString()

}

function deleteCookie(name: string) {
    setCookie(name, '', -1)
}

async function initializeToken(): Promise<string> {

    const xsrfToken = getCookie(tokenCookieName)

    if (xsrfToken) {

        return Promise.resolve(xsrfToken)

    }

    await fetch('/rest/csrf-cookie')
        .catch(() => {
            console.log('Failed to initialize token..')
        })

    return getCookie(tokenCookieName)!

}

async function getHeaders(useToken = true, isMultipart = false): Promise<Record<string, string>> {

    const headers: Record<string, string> = {
        'accept': 'application/json'
    }

    if (!isMultipart) {

        headers[ 'content-type' ] = 'application/json'

    }

    if (useToken) {

        headers[ 'x-xsrf-token' ] = await initializeToken()

    }

    return headers

}

async function cleanUpAndRedirectToLogin() {

    deleteCookie(tokenCookieName)

    window.location.href = '/login'

}

async function handleResponse<Success extends SuccessInterface, Error>(response: Response): Promise<Result<Success['data'], Error>> {

    const responseBody = await response.json()

    if (response.ok && responseBody.success) {

        return ok(readonly(responseBody.data))

    }

    /**
     * If unauthorized redirect to login
     */
    if (responseBody.code === ErrorCode.Unauthenticated) {

        await cleanUpAndRedirectToLogin()

    }

    return error(responseBody)

}

async function get<Success extends SuccessInterface, Error>(url: string): Promise<Result<Success['data'], Error>> {

    const response = await fetch(url, {
        headers: await getHeaders(false)
    })

    return handleResponse<Success, Error>(response)

}

async function post<Success extends SuccessInterface, Error>(url: string, body: BodyInit | null = null, isMultipart = false): Promise<Result<Success['data'], Error>> {

    const response = await fetch(url, {
        method: 'POST',
        body: body,
        credentials: 'include',
        headers: await getHeaders(true, isMultipart)
    })

    return handleResponse<Success, Error>(response)

}

export function useApi() {

    const { updateAuthenticationStatus } = useGlobalState()

    return {
        needsAuthentication() {

            return !updateAuthenticationStatus(getCookie(tokenCookieName) !== null)

        },
        async logout() {

            const response = await post<SuccessInterface, ServerError>('/rest/client/logout')

            if (response.isErr()) {

                alert('Failed to logout... please try again...')

            }

            if (response.isOk()) {

                await cleanUpAndRedirectToLogin()

            }

        },
        login(email: string, password: string) {

            const data = JSON.stringify({ email, password })

            return post<SuccessLoginInterface, ValidationError<'email' | 'password'> | ServerError>('/rest/client/authenticate', data)

        },
        enquiry(data: EnquiryInterface) {

            const formData = new FormData()

            for (const key in data) {

                formData.append(key, data[ key as keyof EnquiryInterface ])

            }

            return post<SuccessInterface, ServerError>(`/rest/api/enquiry`, formData, true)

        },
        fetchTaskData(project: string, tester: string) {

            return get<SubmissionType, ServerError>(`/rest/client/submission/${ project }/${ tester }`)

        },
        project(id: number) {

            return get<ProjectType, ServerError>(`/rest/client/project/${ id }`)

        },
        downloadVideo(videoId: number) {

            window.location.assign(this.prefetchVideoUrl(videoId))

        },
        downloadAllSubmissions() {
            window.location.assign('/rest/client/submissions/download')
        },
        prefetchVideoUrl(videoId: number): string {
            return `/rest/client/video/${ videoId }/prefetch`
        },
        projects() {
            return get<ProjectListingType, ServerError>(`/rest/client/projects`)
        }
    }

}

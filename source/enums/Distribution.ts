export enum Distribution {
    SpaceAround = 'justify-around',
    SpaceBetween = 'justify-between',
    SpaceEvenly = 'justify-evenly',
    End = 'justify-end',
    Start = 'justify-start',
    Center = 'justify-center'
}

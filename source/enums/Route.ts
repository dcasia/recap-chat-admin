export enum Route {
    Home = 'home',
    Login = 'login',
    GetStarted = 'get-started',
    Projects = 'projects',
    Project = 'project',
    Submission = 'submission',
    RecoverPassword = 'recover-password'
}

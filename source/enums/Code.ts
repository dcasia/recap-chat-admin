export enum ErrorCode {
    Success = 0,
    ValidationError = 1,
    ServerError = 2,
    Unauthenticated = 5
}

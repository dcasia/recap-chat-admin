export enum ProjectTaskStatus {
    Completed = 'completed',
    StartedTest = 'started',
    Processing = 'processing',
    Incomplete = 'incomplete',
    Recruited = 'recruited',
    Recruiting = 'recruiting',
}

export enum Color {
    Primary = '#4378FF',
    White = '#FFFFFF',
    Black = '#000000'
}

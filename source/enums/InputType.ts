export enum InputType {
    Email = 'email',
    Text = 'text',
    TextArea = 'textarea',
    Password = 'password',
    Checkbox = 'checkbox',
}

import { ErrorCode } from '../enums/Code'

export interface ApiResponseInterface {
    code: ErrorCode
    success: boolean
}

export interface SuccessInterface<Body = unknown> extends ApiResponseInterface {
    code: ErrorCode.Success
    data: Readonly<Body>
}

export interface ApiErrorResponseInterface extends ApiResponseInterface {
    message: string
}

export interface ValidationError<Property extends string> extends ApiErrorResponseInterface {
    code: ErrorCode.ValidationError,
    errors: {
        [key in Property]?: string[]
    }
}

export interface ServerError extends ApiErrorResponseInterface {
    code: ErrorCode.ServerError,
}

export interface SuccessLoginInterface extends SuccessInterface {
    projectId: number
}

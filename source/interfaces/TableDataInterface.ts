export interface TableDataInterface {
    withCheckboxes: boolean
    headers: (string | null)[],
    body: Record<string, unknown>[]
}

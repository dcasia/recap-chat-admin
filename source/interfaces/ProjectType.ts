import { SuccessInterface } from './ApiInterface'
import { ProjectTaskStatus } from '../enums/ProjectTaskStatus'

export interface TaskInterface {
    id: string,
    header: string,
    type: string,
    content: string
}

export interface TesterInterface {
    id: number,
    age: number,
    city: string,
    firstName: string,
    lastName: string,
    gender: string,
    // submission?: SubmissionType
}

export type SubmissionType = SuccessInterface<{
    tasks: TaskInterface[],
    tester: {
        firstName: string,
        lastName: string,
        device: DeviceInfoInterface,
        submissionDate: string,
        submissionTime: string
    },
    video: {
        id: number,
        duration: number,
        width: number,
        height: number
    },
}>

export type ProjectListingType = SuccessInterface<{
    id: number,
    name: string,
    seats: number,
    completed: number,
    recruited: number,
    createdAt: string,
}[]>

export type ProjectType = SuccessInterface<{
    id: number,
    name: string,
    seats: number,
    // tasks: TaskInterface[]
    testers: NewTesterInfoInterface[]
}>

export interface NewTaskInterface {
    id: number
    stoppedAt: number
}

interface DeviceInfoInterface {
    model: string
    name: string
    systemName: string
    systemVersion: string
}

interface NewTesterInfoInterface {
    age: number
    city: string
    avatar?: string
    firstName: string
    gender: string
    id: number
    lastName: string,
    status: ProjectTaskStatus,
    video?: {
        id: number,
        duration: number,
    }
}

export interface TesterInfoInterface {
    age: number
    city: string
    firstName: string
    gender: string
    id: number
    lastName: string,
    submission?: {
        id: number,
        duration: number,
        device: DeviceInfoInterface
    }
}

export interface EnquiryInterface {
    name: string,
    email: string,
    jobTitle: string,
    projectDetail: string,
    participants: string,
    length: string
}
